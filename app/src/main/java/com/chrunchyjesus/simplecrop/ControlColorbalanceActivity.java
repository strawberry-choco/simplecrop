package com.chrunchyjesus.simplecrop;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;

import com.chrunchyjesus.simplecrop.utils.ImgProcessingUtils;

public class ControlColorbalanceActivity extends ProcessmodeActivity {
    Bitmap img;
    SeekBar c2r;
    SeekBar m2g;
    SeekBar y2b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_colorbalance);

        imgview = findViewById(R.id.imgview);
        img = getIntentSourceImg();
        imgview.setImageBitmap(img.copy(Bitmap.Config.ARGB_8888, true));

        setupSeekbar();
    }

    private void setupSeekbar() {
        c2r = findViewById(R.id.c2r_seekBar);
        m2g = findViewById(R.id.m2g_seekBar);
        y2b = findViewById(R.id.y2b_seekBar);

        c2r.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Settings.ColorBalance.c2r = 255 + -1*c2r.getProgress();

                Bitmap bitmap = colorBalance(img.copy(Bitmap.Config.ARGB_8888, true));

                imgview.setImageBitmap(bitmap);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}
        });

        m2g.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Settings.ColorBalance.m2g = 255 + -1*m2g.getProgress();

                Bitmap bitmap = colorBalance(img.copy(Bitmap.Config.ARGB_8888, true));

                imgview.setImageBitmap(bitmap);
//                mListener.onBrightnessChange(seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}
        });

        y2b.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Settings.ColorBalance.y2b = 255 + -1*y2b.getProgress();

                Bitmap bitmap = colorBalance(img.copy(Bitmap.Config.ARGB_8888, true));

                imgview.setImageBitmap(bitmap);
//                mListener.onBrightnessChange(seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}
        });
    }

    final private static class Settings {
        final static class ColorBalance {
            public static float c2r = 255;
            public static float m2g = 255;
            public static float y2b = 255;
            public static float defaultc2r = 255;
            public static float defaultm2g = 255;
            public static float defaulty2b = 255;

            public static void resetC2R() {
                c2r = defaultc2r;
            }
            public static void resetM2G() {
                m2g = defaultm2g;
            }
            public static void resetY2B() {
                y2b = defaulty2b;
            }
        }
    }

    public void onResetC2R(View view) {
        Settings.ColorBalance.resetC2R();
        syncSettings();

        Bitmap bitmap = colorBalance(img.copy(Bitmap.Config.ARGB_8888, true));

        imgview.setImageBitmap(bitmap);
    }
    public void onResetM2G(View view) {
        Settings.ColorBalance.resetM2G();
        syncSettings();

        Bitmap bitmap = colorBalance(img.copy(Bitmap.Config.ARGB_8888, true));

        imgview.setImageBitmap(bitmap);
    }
    public void onResetY2B(View view) {
        Settings.ColorBalance.resetY2B();
        syncSettings();

        Bitmap bitmap = colorBalance(img.copy(Bitmap.Config.ARGB_8888, true));

        imgview.setImageBitmap(bitmap);
    }

    public void syncSettings() {
        c2r.setProgress(255 + -1*(int) Settings.ColorBalance.c2r);
        m2g.setProgress(255 + -1*(int) Settings.ColorBalance.m2g);
        y2b.setProgress(255 + -1*(int) Settings.ColorBalance.y2b);
    }

    private static Bitmap colorBalance(Bitmap img) {
        int[] pixelBuffer = ImgProcessingUtils.getPixelArrayFromBitmap(img);

        for (int k = 0; k < pixelBuffer.length; k++) {
            int[] argb = ImgProcessingUtils.pixelToARGBArray(pixelBuffer[k]);
            float red = 255.0f / Settings.ColorBalance.c2r * (float)argb[1];
            float green = 255.0f / Settings.ColorBalance.m2g * (float)argb[2];
            float blue = 255.0f / Settings.ColorBalance.y2b * (float)argb[3];

            red = ImgProcessingUtils.trimBoundaries((int) red);
            green = ImgProcessingUtils.trimBoundaries((int) green);
            blue = ImgProcessingUtils.trimBoundaries((int) blue);

            argb[1] = (int) red;
            argb[2] = (int) green;
            argb[3] = (int) blue;

            pixelBuffer[k] = ImgProcessingUtils.ARGBArrayToPixel(argb);
        }

        return Bitmap.createBitmap(pixelBuffer, img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
    }
}
