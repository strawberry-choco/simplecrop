package com.chrunchyjesus.simplecrop;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.chrunchyjesus.simplecrop.dialogs.AboutusDialog;
import com.chrunchyjesus.simplecrop.utils.ImgUtils;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    final static int PICK_FROM_MANAGER = 1;
    final static int PICK_FROM_CAMERA = 2;
    final static int PICK_FROM_RECENT = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Preferences.applySettings(this);
        setContentView(R.layout.activity_main);

        if(!Permissions.hasWritePermission(this)) {
            Permissions.requestWritePermission(this);
        }
    }

    public void onPickFromFileClick(View view) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");

        PackageManager packageManager = getPackageManager();
        List activities = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if(activities.size() <= 0) {
            Toast.makeText(this, R.string.no_filepicker, Toast.LENGTH_SHORT).show();
            return;
        }

        startActivityForResult(intent, PICK_FROM_MANAGER);
    }

    public void onPickFromCameraClick(View view) {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        PackageManager packageManager = getPackageManager();
        List activities = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if(activities.size() <= 0) {
            Toast.makeText(this, R.string.no_camera, Toast.LENGTH_SHORT).show();
            return;
        }

        startActivityForResult(intent, PICK_FROM_CAMERA);
    }

    public void onPickFromRecentClick(View view) {

    }

    public void onSettingsClick(View view) {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(intent);
    }

    public void onAboutUsClick(View view) {
        DialogFragment newFragment = new AboutusDialog();
        newFragment.show(getSupportFragmentManager(), "aboutus");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_OK) {
            handleActivityResultError(requestCode);
            return;
        }

        handleActivityResultOkay(requestCode, resultCode, data);
    }

    private void handleActivityResultError(int requestCode) {
        String msg;
        switch (requestCode) {
            case PICK_FROM_MANAGER:
                msg = this.getString(R.string.activityresult_empty_gallery);
                break;
            case PICK_FROM_CAMERA:
                msg = this.getString(R.string.activityresult_empty_camera);
                break;
            default:
                msg = "Unknown error";
                break;
        }
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void handleActivityResultOkay(int requestCode, int resultCode, Intent data) {
        Intent intent;
        Uri uri = null;

        switch (requestCode) {
            case PICK_FROM_MANAGER:
                uri = data.getData();
                break;
            case PICK_FROM_CAMERA:
                uri = ImgUtils.getImageUri(this, (Bitmap) data.getExtras().get("data"));
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }

        if(uri == null) return;

        intent = new Intent(MainActivity.this, ProcessorActivity.class);
        intent.putExtra(ProcessorActivity.IMGSRC, uri);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Permissions.checkRequestPermissionsSuccess(requestCode, permissions, grantResults);

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
