package com.chrunchyjesus.simplecrop;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.chrunchyjesus.simplecrop.utils.ImgUtils;

import java.io.IOException;

public class ProcessmodeActivity extends AppCompatActivity {
    final static String SOURCEIMG = "source";
    protected ImageView imgview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Preferences.applySettings(this);

        super.onCreate(savedInstanceState);

        setupActionbar();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()){
            overridePendingTransition(0, 0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accept:
                Intent intent = new Intent(this, ProcessorActivity.class);
                Bitmap img = ImgUtils.getBitmapFromImageview(imgview);

                intent.putExtra(ProcessorActivity.IMGSRC, ImgUtils.getImageUri(this, img));
                setResult(RESULT_OK, intent);
                this.finish();
                return true;
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                setResult(RESULT_CANCELED);
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.processmode, menu);
        return true;
    }

    private void setupActionbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    protected Bitmap getIntentSourceImg() {
        Bitmap img = null;

        try {
            img = ImgUtils.getBitmapFromUri(getContentResolver(), (Uri) getIntent().getParcelableExtra(SOURCEIMG));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return img;
    }
}
