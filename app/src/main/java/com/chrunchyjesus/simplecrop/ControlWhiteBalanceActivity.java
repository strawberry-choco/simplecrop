package com.chrunchyjesus.simplecrop;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.chrunchyjesus.simplecrop.utils.ImgProcessingUtils;

public class ControlWhiteBalanceActivity extends ProcessmodeActivity {
    Bitmap img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_white_balance);

        imgview = findViewById(R.id.imageview);
        img = getIntentSourceImg();
        imgview.setImageBitmap(img.copy(Bitmap.Config.ARGB_8888, true));

//        final Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        imgview.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event){
//                int x = (int)event.getX();
//                int y = (int)event.getY();
                Matrix inverse = new Matrix();
                imgview.getImageMatrix().invert(inverse);
                float[] touchPoint = new float[] {event.getX(), event.getY()};
                inverse.mapPoints(touchPoint);
                int x = Integer.valueOf((int)touchPoint[0]);
                int y = Integer.valueOf((int)touchPoint[1]);

                int pixel = img.getPixel(x,y);

                imgview.setImageBitmap(performWhitebalance(img.copy(Bitmap.Config.ARGB_8888, true), pixel));

                //then do what you want with the pixel data, e.g
//                int redValue = Color.red(pixel);
//                int blueValue = Color.blue(pixel);
//                int greenValue = Color.green(pixel);
                return false;
            }
        });
    }

    private Bitmap performWhitebalance(Bitmap img, int whitespotPixel) {
        int[] pixelBuffer = ImgProcessingUtils.getPixelArrayFromBitmap(img);
        int redValue = Color.red(whitespotPixel);
        int blueValue = Color.blue(whitespotPixel);
        int greenValue = Color.green(whitespotPixel);

        float grey = (redValue+greenValue+blueValue) / 3;


        for (int k = 0; k < pixelBuffer.length; k++) {
            int[] argb = ImgProcessingUtils.pixelToARGBArray(pixelBuffer[k]);
            float red = grey / redValue * (float)argb[1];
            float green = grey / greenValue * (float)argb[2];
            float blue = grey / blueValue * (float)argb[3];

            red = ImgProcessingUtils.trimBoundaries((int) red);
            green = ImgProcessingUtils.trimBoundaries((int) green);
            blue = ImgProcessingUtils.trimBoundaries((int) blue);

            argb[1] = (int) red;
            argb[2] = (int) green;
            argb[3] = (int) blue;

            pixelBuffer[k] = ImgProcessingUtils.ARGBArrayToPixel(argb);
        }

        return Bitmap.createBitmap(pixelBuffer, img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
    }
}
