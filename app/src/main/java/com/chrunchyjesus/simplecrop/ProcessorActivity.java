package com.chrunchyjesus.simplecrop;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.chrunchyjesus.simplecrop.dialogs.SaveFileActivity;
import com.chrunchyjesus.simplecrop.utils.ImgUtils;
import com.chrunchyjesus.simplecrop.utils.IntentUtils;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;

public class ProcessorActivity extends AppCompatActivity{
    public static final String IMGSRC = "IMGSRC";
    private static final int REQ_COLORBALANCE = 1;
    private static final int REQ_WHITEBALANCE = 2;
    private static final int REQ_CONTRAST = 3;
    private static final int REQ_FILTER = 4;
    private static final int REQ_BRIGHTNESS = 5;
    private static final int REQ_SAVE = 6;
    private static final int REQ_SATURATION = 7;
    private ImageView imgview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Preferences.applySettings(this);
        setContentView(R.layout.activity_processor);

        // get image
        Bitmap image = null;

        try {
            image = ImgUtils.getBitmapFromUri(getContentResolver(), (Uri) getIntent().getParcelableExtra(IMGSRC));
        } catch (IOException e) {
            Toast.makeText(this, R.string.io_exception_reading, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        imgview = findViewById(R.id.img);
        imgview.setImageBitmap(image);
    }

    public void onSaveClick(View view) {
        Bitmap image = ImgUtils.getBitmapFromImageview(imgview);
        Uri uri = ImgUtils.getImageUri(this, image);

        Intent intent = new Intent(ProcessorActivity.this, SaveFileActivity.class);
        intent.putExtra(ControlFilterActivity.SOURCEIMG, uri);
        startActivityForResult(intent, REQ_SAVE);
    }

    public void onMirrorClick(View view) {
        ImageView imageView = findViewById(R.id.img);
        BitmapDrawable img2;

        switch(view.getId()) {
            case R.id.mirror_horizontal:
                img2 = Engine.flipHorizontal((BitmapDrawable)imageView.getDrawable());
                break;
            case R.id.mirror_vertical:
                img2 = Engine.flipVertical((BitmapDrawable)imageView.getDrawable());
                break;
            default:
                img2 = (BitmapDrawable)imageView.getDrawable();
        }

        imageView.setImageBitmap(img2.getBitmap());
    }

    public void onTransformClick(View view) {
        Bitmap image = ImgUtils.getBitmapFromImageview(imgview);
        Uri mDestinationUri = Uri.fromFile(new File(getCacheDir(), "img.png"));
        Uri uri = ImgUtils.getImageUri(this, image);

        UCrop uCrop = UCrop.of(uri, mDestinationUri);
        uCrop.start(this);
    }

    public void onFilterClick(View view) {
        Bitmap image = ImgUtils.getBitmapFromImageview(imgview);
        Uri uri = ImgUtils.getImageUri(this, image);

        Intent intent = IntentUtils.createAnimationlessIntent(ProcessorActivity.this, ControlFilterActivity.class);
        intent.putExtra(ControlFilterActivity.SOURCEIMG, uri);
        startActivityForResult(intent, REQ_FILTER);
    }

    public void onBrightnessClick(View view) {
        Bitmap image = ImgUtils.getBitmapFromImageview(imgview);
        Uri uri = ImgUtils.getImageUri(this, image);

        Intent intent = IntentUtils.createAnimationlessIntent(ProcessorActivity.this, ControlBrightnessActivity.class);
        intent.putExtra(ControlBrightnessActivity.SOURCEIMG, uri);
        startActivityForResult(intent, REQ_BRIGHTNESS);
    }

    public void onColorbalanceClick(View view) {
        Bitmap image = ImgUtils.getBitmapFromImageview(imgview);
        Uri uri = ImgUtils.getImageUri(this, image);

        Intent intent = IntentUtils.createAnimationlessIntent(ProcessorActivity.this, ControlColorbalanceActivity.class);
        intent.putExtra(ControlColorbalanceActivity.SOURCEIMG, uri);
        startActivityForResult(intent, REQ_COLORBALANCE);
    }

    public void onWhitebalanceClick(View view) {
        Bitmap image = ImgUtils.getBitmapFromImageview(imgview);
        Uri uri = ImgUtils.getImageUri(this, image);

        Intent intent = IntentUtils.createAnimationlessIntent(ProcessorActivity.this, ControlWhiteBalanceActivity.class);
        intent.putExtra(ControlWhiteBalanceActivity.SOURCEIMG, uri);
        startActivityForResult(intent, REQ_WHITEBALANCE);
    }

    public void onContrastClick(View view) {
        Bitmap image = ImgUtils.getBitmapFromImageview(imgview);
        Uri uri = ImgUtils.getImageUri(this, image);

        Intent intent = IntentUtils.createAnimationlessIntent(ProcessorActivity.this, ControlContrastActivity.class);
        intent.putExtra(ControlContrastActivity.SOURCEIMG, uri);
        startActivityForResult(intent, REQ_CONTRAST);
    }

    public void onSaturationClick(View view) {
        Bitmap image = ImgUtils.getBitmapFromImageview(imgview);
        Uri uri = ImgUtils.getImageUri(this, image);

        Intent intent = IntentUtils.createAnimationlessIntent(ProcessorActivity.this, ControlSaturationActivity.class);
        intent.putExtra(ControlSaturationActivity.SOURCEIMG, uri);
        startActivityForResult(intent, REQ_SATURATION);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_CANCELED) {
            return;
        }
//        check for error
        if(resultCode != RESULT_OK) {
            String msg = "";

            switch (requestCode) {
                case UCrop.REQUEST_CROP:
                    msg = "Error transforming image";
                    break;
                case REQ_COLORBALANCE:
                    msg = "Error changing color balance";
                    break;
                default:
                    msg = "Unknown error";
                    break;
            }

            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
            return;
        }

//        execute based on requestcode
        switch (requestCode) {
            case UCrop.REQUEST_CROP:
                if(data == null) break;

                final Uri imageUri = UCrop.getOutput(data);
                if (imageUri == null || !imageUri.getScheme().equals("file")) break;

                try {
                    Bitmap img = ImgUtils.getBitmapFromUri(getContentResolver(), imageUri);
                    ImageView imageview = findViewById(R.id.img);
                    imageview.setImageBitmap(img);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case REQ_COLORBALANCE: // fallthrough
            case REQ_CONTRAST: // fallthrough
            case REQ_BRIGHTNESS: //fallthrough
            case REQ_FILTER: // fallthrough
            case REQ_SATURATION: // fallthrough
            case REQ_WHITEBALANCE:
                Uri uri = (Uri) data.getExtras().get(ProcessorActivity.IMGSRC);
                try {
                    Bitmap img = ImgUtils.getBitmapFromUri(getContentResolver(), uri);

                    imgview.setImageBitmap(img);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case REQ_SAVE:
                File path = (File) data.getExtras().get("savepath");
                String name = data.getStringExtra("name");
                ImgUtils.saveImage(path, name, ImgUtils.getBitmapFromImageview(imgview));
                Log.d("save", "saved in processoractivity");
                Toast.makeText(this, "img saved", Toast.LENGTH_SHORT).show();
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
}
