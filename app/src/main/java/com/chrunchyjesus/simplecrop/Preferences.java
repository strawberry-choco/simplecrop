package com.chrunchyjesus.simplecrop;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Environment;
import android.preference.PreferenceManager;

import java.io.File;
import java.util.Locale;

public final class Preferences {
    public static void applySettings(Activity activity) {
        applyThemeSetting(activity);
        applyLocaleSetting(activity);
    }

    public static void applyLocaleSetting(Activity activity) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity);
        String lang = sharedPref.getString("lang", "");
        Locale locale;

        switch(lang){
            case "German":
                locale = new Locale("de");
                break;
            default:
                locale = new Locale("en");
                break;
        }

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        activity.getApplicationContext().getResources().updateConfiguration(config, null);
    }

    public static void applyThemeSetting(Activity activity) {
        activity.setTheme(getTheme(activity));
    }

    public static int getTheme(Activity activity) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity);
        int theme;

        switch(sharedPref.getString("theme", "")) {
            case "Dark":
                theme = android.R.style.Theme_DeviceDefault_NoActionBar;
                break;
            default:
                theme = android.R.style.Theme_DeviceDefault_Light_NoActionBar;
                break;
        }

        return theme;
    }

    public static int getDialogTheme(Activity a) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(a);
        int theme;

        switch(sharedPref.getString("theme", "")) {
            case "Dark":
                theme = android.R.style.Theme_DeviceDefault_Dialog_NoActionBar;
                break;
            default:
                theme = android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar;
                break;
        }

        return theme;
    }

    public static String getDefaultSaveFolder(Activity activity) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity);
        String def = Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_PICTURES;
        String folder = sharedPref.getString("savepath", def);

        if(folder.equals("-")) {
            folder = def;
        }

        return folder;
    }

    public static String getDefaultNameSuffix(Activity activity) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity);
        String suffix = sharedPref.getString("suffix", "");

        return suffix;
    }

    public static String getDefaultNamePrefix(Activity activity) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity);
        String prefix = sharedPref.getString("prefix", "");

        return prefix;
    }
}
