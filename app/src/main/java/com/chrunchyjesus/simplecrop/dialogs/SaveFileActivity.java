package com.chrunchyjesus.simplecrop.dialogs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.chrunchyjesus.simplecrop.Preferences;
import com.chrunchyjesus.simplecrop.ProcessorActivity;
import com.chrunchyjesus.simplecrop.R;

import java.io.File;

import yogesh.firzen.filelister.FileListerDialog;
import yogesh.firzen.filelister.OnFileSelectedListener;

public class SaveFileActivity extends AppCompatActivity {
    private String defaultFolder;
    private int theme;
    private File savepath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Preferences.applySettings(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_file);

        defaultFolder = Preferences.getDefaultSaveFolder(this);
        String prefix = Preferences.getDefaultNamePrefix(this);
        String suffix = Preferences.getDefaultNameSuffix(this);
        theme = Preferences.getDialogTheme(this);

        Button pickFolder = findViewById(R.id.pick_folder);
        EditText name = findViewById(R.id.file_name);

        Log.d("save", defaultFolder);

        pickFolder.setText(defaultFolder);
        name.setText(prefix + suffix);
    }

    public void onPickfolderClick(View view) {
        FileListerDialog filelister = FileListerDialog.createFileListerDialog(this, theme);
        filelister.setOnFileSelectedListener(new OnFileSelectedListener() {
            @Override
            public void onFileSelected(File file, String path) {
                Log.d("save", file.toString());
                Log.d("save", path);
                savepath = file;
            }
        });

        filelister.setDefaultDir(defaultFolder);
        filelister.setFileFilter(FileListerDialog.FILE_FILTER.DIRECTORY_ONLY);
        filelister.show();
    }

    public void onAcceptClick(View view) {
        Intent intent = new Intent(this, ProcessorActivity.class);

        EditText et = findViewById(R.id.file_name);
        String name = et.getText().toString() + ".png";
        Log.d("save", name);

        intent.putExtra("savepath", savepath);
        intent.putExtra("name", name);

        setResult(RESULT_OK, intent);
        this.finish();
    }
}
