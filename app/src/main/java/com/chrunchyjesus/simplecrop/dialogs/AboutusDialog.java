package com.chrunchyjesus.simplecrop.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.chrunchyjesus.simplecrop.R;

/**
 * Created by chrunchyjesus on 10/25/17.
 */

public class AboutusDialog extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String syncConnPref = sharedPref.getString("theme", "");
        AlertDialog.Builder builder;
        switch(syncConnPref) {
            case "Dark":
                builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_DeviceDefault_Dialog_NoActionBar);
                break;
            default: builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
                break;
        }
        builder.setMessage(R.string.about_us_content)
            .setTitle("About us")
            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    AboutusDialog.this.getDialog().cancel();
                }
            });
        // Create the AlertDialog object and return it
        return builder.create();
    }

}
