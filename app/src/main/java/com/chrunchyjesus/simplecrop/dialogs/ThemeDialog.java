package com.chrunchyjesus.simplecrop.dialogs;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThemeDialog extends DialogFragment {


    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Theme");
        builder.setItems(new String[]{"Light","Dark"}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch(which) {
                    case 0:
                        //getView().setBackgroundColor(Color.WHITE);
                        Log.d("Test","light");
                        break;
                    case 1:
                        //getView().setBackgroundColor(Color.DKGRAY);
                        Log.d("Test","dark");
                        break;
                }
            }

        });

        // Create the AlertDialog object and return it
        return builder.create();
    }


}
