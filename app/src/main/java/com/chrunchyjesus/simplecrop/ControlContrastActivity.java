package com.chrunchyjesus.simplecrop;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.SeekBar;

import com.chrunchyjesus.simplecrop.utils.ImgProcessingUtils;

public class ControlContrastActivity extends ProcessmodeActivity {
    Bitmap img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_contrast);

        imgview = findViewById(R.id.imageView);
        img = getIntentSourceImg();
        imgview.setImageBitmap(img.copy(Bitmap.Config.ARGB_8888, true));

        setupSeekbar();
    }

    public void setupSeekbar() {
        SeekBar sk = findViewById(R.id.contrastSeekBar);

        sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int contrastValue = seekBar.getProgress() - 100;
                Bitmap bitmap = adjustContrast(img.copy(Bitmap.Config.ARGB_8888, true), contrastValue);
                imgview.setImageBitmap(bitmap);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}
        });
    }

    private Bitmap adjustContrast(Bitmap img, int contrastValue) {
        int[] pixels = ImgProcessingUtils.getPixelArrayFromBitmap(img);
        float c = (100 + contrastValue) * 0.01f;

        for (int i=0; i < pixels.length; i++) {
            int a = Color.alpha(pixels[i]);
            int r = Color.red(pixels[i]);
            int g = Color.green(pixels[i]);
            int b = Color.blue(pixels[i]);
//            int a = (pixels[i] & 0xFF000000) >> (6 * 4);
//            int r = (pixels[i] & 0x00FF0000) >> (4 * 4);
//            int g = (pixels[i] & 0x0000FF00) >> (2 * 4);
//            int b = (pixels[i] & 0x000000FF);

            r = (int) (((((r / 255.0f) - 0.5f) * c) + 0.5f) * 255.0f);
            g = (int) (((((g / 255.0f) - 0.5f) * c) + 0.5f) * 255.0f);
            b = (int) (((((b / 255.0f) - 0.5f) * c) + 0.5f) * 255.0f);

            r = ImgProcessingUtils.trimBoundaries(r);
            g = ImgProcessingUtils.trimBoundaries(g);
            b = ImgProcessingUtils.trimBoundaries(b);

            pixels[i] = Color.argb(a, r, g, b);

//            pixels[i] = (
//                    (a << (6 * 4)) +
//                            (r << (4 * 4)) +
//                            (g << (2 * 4)) +
//                            b
//            );
        }

        return Bitmap.createBitmap(pixels, img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
    }
}
