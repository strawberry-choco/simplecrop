package com.chrunchyjesus.simplecrop;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.chrunchyjesus.simplecrop.utils.ImgProcessingUtils;
import com.chrunchyjesus.simplecrop.utils.ImgUtils;

public class ControlSaturationActivity extends ProcessmodeActivity {
    Bitmap img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_saturation);

        img = getIntentSourceImg();
        imgview = findViewById(R.id.imageView);
        imgview.setImageBitmap(img.copy(Bitmap.Config.ARGB_8888, true));

        setupSeekbar();
    }

    public void setupSeekbar() {
        SeekBar sk = findViewById(R.id.saturation_seekbar);

        sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int saturationValue = seekBar.getProgress();
                Bitmap bitmap = adjustSaturation(saturationValue);
                imgview.setImageBitmap(bitmap);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}
        });
    }

    private Bitmap adjustSaturation(int val) {
        float s = val * 0.01f;

        int[] pixels = ImgProcessingUtils.getPixelArrayFromBitmap(img);

        for(int i = 0; i < pixels.length; i++){
            pixels[i] = applySaturation(pixels[i], s);
        }

        return Bitmap.createBitmap(pixels, img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
    }

    private int applySaturation(int pixel, float amount) {
        int a = Color.alpha(pixel);
        float[] hsl = new float[3];

        Color.colorToHSV(pixel, hsl);

        hsl[1] *= amount;

        return Color.HSVToColor(a, hsl);
    }
}
