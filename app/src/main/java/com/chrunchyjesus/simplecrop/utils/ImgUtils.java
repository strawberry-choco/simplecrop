package com.chrunchyjesus.simplecrop.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.widget.ImageView;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by chrunchyjesus on 14.02.18.
 */

public class ImgUtils {
    public static Uri getImageUri(Context inContext, Bitmap img) {
        File file = saveImage(inContext.getCacheDir(), "tmp.png", img);

        return Uri.fromFile(file);
    }

    public static File saveImage(File path, String name, Bitmap img) {
//        TODO: ask for write external storage at runtime if not granted
        File file = new File(path, name);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            // Use the compress method on the BitMap object to write image to the OutputStream
            img.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }

    public static Bitmap getBitmapFromUri(ContentResolver resolver, Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                resolver.openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public static Bitmap getBitmapFromImageview(ImageView view) {
        return ((BitmapDrawable) view.getDrawable()).getBitmap();
    }
}
