package com.chrunchyjesus.simplecrop;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.Log;

import com.chrunchyjesus.simplecrop.interfaces.IRender;

import java.util.HashMap;

public class Engine {
    private boolean IN_TRANSACTION = false;
    private Bitmap img;
    private Bitmap tmpimg;

    public Engine(Bitmap img) {
        setImage(img);
    }

    public Engine startTransaction() {
        Log.d("transaction", "start, " + String.valueOf(this.IN_TRANSACTION));
        if(this.IN_TRANSACTION) return this;
        this.IN_TRANSACTION = true;
        tmpimg = img.copy(Bitmap.Config.ARGB_8888, false);

        return this;
    }

    public Engine commit () {
        Log.d("transaction", "commit, " + String.valueOf(this.IN_TRANSACTION));
        if(!this.IN_TRANSACTION) return this;
        this.IN_TRANSACTION = false;
        tmpimg = null;

        return this;
    }

    public Engine revert() {
        Log.d("transaction", "revert, " + String.valueOf(this.IN_TRANSACTION));
        if(!this.IN_TRANSACTION) return this;
        this.IN_TRANSACTION = false;
        img = tmpimg.copy(Bitmap.Config.ARGB_8888, true);

        return this;
    }

    @NonNull
    static BitmapDrawable flipHorizontal(BitmapDrawable d) {
        Matrix m = new Matrix();
        m.preScale(-1, 1);
        Bitmap src = d.getBitmap();
        Bitmap dst = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), m, false);
        dst.setDensity(DisplayMetrics.DENSITY_DEFAULT);
        return new BitmapDrawable(dst);
    }
    @NonNull
    static BitmapDrawable flipVertical(BitmapDrawable d) {
        Matrix m = new Matrix();
        m.preScale(1, -1);
        Bitmap src = d.getBitmap();
        Bitmap dst = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), m, false);
        dst.setDensity(DisplayMetrics.DENSITY_DEFAULT);
        return new BitmapDrawable(dst);
    }

    private Bitmap render(ColorMatrix m) {
        Bitmap tmp = Bitmap.createBitmap(this.img);
        Bitmap bitmap = Bitmap.createBitmap(tmp.getWidth(),
                tmp.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(m));
        canvas.drawBitmap(tmp, 0, 0, paint);

        return bitmap;
    }

    private Bitmap render(Bitmap img, IRender r, HashMap args) {
        Bitmap nimg = img.copy(Bitmap.Config.ARGB_8888, true);

        int[] arr = new int[nimg.getWidth() * nimg.getHeight()];
        nimg.getPixels(arr, 0, nimg.getWidth(), 0, 0, nimg.getWidth(), nimg.getHeight());
        int[] arr2 = r.render(arr, args);

        //Initialize the bitmap, with the replaced color
        nimg = Bitmap.createBitmap(arr2, nimg.getWidth(), nimg.getHeight(), Bitmap.Config.ARGB_8888);

        return nimg;
    }

    public Engine brightness(int value) {
        float b = value * 0.01f;

        img = render(new ColorMatrix(new float[] {
                b,  0,  0,  0, 0,
                0,  b,  0,  0, 0,
                0,  0,  b,  0, 0,
                0,  0,  0,  1, 0
        }));

        return this;
    }

    public Engine setImage(Bitmap img) {
        this.img = img.copy(Bitmap.Config.ARGB_8888, true);

        return this;
    }

    public Bitmap getImage() {
        return this.img;
    }

    protected final static class Settings {
        /**
         * @description value needs to be between -100 and 100 (including -100 and 100)
         */
        public static float CONTRAST = 0;
        public static float BRIGHTNESS = 0;
    }
}
