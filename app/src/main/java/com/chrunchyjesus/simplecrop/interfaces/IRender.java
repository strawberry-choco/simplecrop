package com.chrunchyjesus.simplecrop.interfaces;

import java.util.HashMap;

public interface IRender {
    public int[] render(int[] arr, HashMap args);
}
