# Simplecrop
> A simple open source photo editor for Android without any unnecessary permissions and tracking.

## Features

### General
- Light and dark theme
- Name prefix and suffix for saved files
- Choose screen brightness when editing images

### Editing
- Transformation (crop, rotate, mirror, scale)
- White balance (currently buggy)
- Image brightness
- Adjust contrast
- Many filters (greyscale, inversion, ...)

## Currently Planned Features
- Color balance
- Saturation
- Color temperature
- Show information about the image (and editing them)

## Note
Expect spaghetti code and cringe when reading our code.
